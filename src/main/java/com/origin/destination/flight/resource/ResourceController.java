package com.origin.destination.flight.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.origin.destination.flight.entity.OriginModel;
import com.origin.destination.flight.service.OriginService;

@RestController
@RequestMapping("/home")
public class ResourceController {

	@Autowired
	private OriginService originService;

	@GetMapping("/search/{departsFrom}/{destination}/{departDate}")
	@ResponseStatus(HttpStatus.OK)
	public List<OriginModel> detailsByOrigin(@PathVariable("departsFrom") String departsFrom,
			@PathVariable("destination") String destination, @PathVariable("departDate") String departDate) {
		List<OriginModel> searchList = originService.detailsByOrigin(departsFrom, destination, departDate);
		return searchList;
	}

	@GetMapping("/search/number/{fNumber}/{departureDt}")
	@ResponseStatus(HttpStatus.OK)
	public List<OriginModel> detailsByNumber(@PathVariable("fNumber") String fNumber,
			@PathVariable("departureDt") String departureDt) {
		List<OriginModel> searchList = originService.detailsByNumber(fNumber, departureDt);
		return searchList;
	}
}
