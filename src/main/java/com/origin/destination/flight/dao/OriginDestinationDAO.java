package com.origin.destination.flight.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.origin.destination.flight.entity.OriginModel;

@Repository
public interface OriginDestinationDAO extends JpaRepository<OriginModel, Long> {

	@Query("Select om from OriginModel om where om.departsFrom = :departsFrom and om.destination = :destination and om.departDate BETWEEN :fromDate AND :toDate")
	List<OriginModel> getDetailsByOrigin(@Param("departsFrom") String departsFrom, @Param("destination") String destination,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query("Select om from OriginModel om where om.fNumber = :fNumber and om.departDate BETWEEN :fromDate AND :toDate")
	List<OriginModel> getDetailsByNumber(@Param("fNumber") String fNumber, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate);

}
