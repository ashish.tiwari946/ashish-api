package com.origin.destination.flight.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.origin.destination.flight.dao.OriginDestinationDAO;
import com.origin.destination.flight.entity.OriginModel;

@Service
public class OriginService {

	@Autowired
	private OriginDestinationDAO repo;

	public List<OriginModel> detailsByOrigin(String departsFrom, String destination, String departsDate) {
		String departDate = null;
		try {
			departDate = modifyDateLayout(departsDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date toDate = null;
		try {
			toDate = getDatePlus(departDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date fromDate = null;
		try {
			fromDate = getDateMinus(departDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<OriginModel> flightsList = repo.getDetailsByOrigin(departsFrom, destination, fromDate, toDate);
		return flightsList;
	}

	public List<OriginModel> detailsByNumber(String fNumber, String departureDt) {
		String departDate = null;
		try {
			departDate = modifyDateLayout(departureDt);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date toDate = null;
		try {
			toDate = getDatePlus(departDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date fromDate = null;
		try {
			fromDate = getDateMinus(departDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<OriginModel> flightsList = repo.getDetailsByNumber(fNumber, fromDate, toDate);
		return flightsList;
	}

	private Date getDatePlus(String departDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		String datePlus;
		Calendar c = Calendar.getInstance();
		c.setTime(format.parse(departDate));
		c.add(Calendar.DATE, 1); // number of days to add
		datePlus = format.format(c.getTime());
		Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS").parse(datePlus);
		return date1;
	}

	private Date getDateMinus(String departDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		Calendar cal = Calendar.getInstance();
		cal.setTime(format.parse(departDate));
		cal.add(Calendar.DATE, -1);
		String dateMinus = format.format(cal.getTime());
		Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS").parse(dateMinus);
		return date1;
	}

	private String modifyDateLayout(String inputDate) throws ParseException {
		inputDate = inputDate.replace(".000Z", "");
		Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(inputDate);
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

}
