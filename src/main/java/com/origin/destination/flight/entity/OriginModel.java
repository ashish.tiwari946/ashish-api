package com.origin.destination.flight.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "detail_field")
public class OriginModel {

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "flight_number")
	private String fNumber;

	@Column(name = "carrier")
	private String carrier;

	@Column(name = "origin")
	private String departsFrom;

	@Column(name = "departure")
	private Date departDate;

	@Column(name = "destination")
	private String destination;

	@Column(name = "arrival")
	private Date arrival;

	@Column(name = "aircraft")
	private String airlines;

	@Column(name = "distance")
	private int distance;

	@Column(name = "travel_time")
	private String totalTime;

	@Column(name = "status")
	private String status;
}
